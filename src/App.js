import React from 'react'
import {BrowserRouter,Switch,Route} from 'react-router-dom'
import Home from './Components/Home'
import Board from './Components/Board'
import BoardList from './Components/BoardList'
import NotFound from './Components/NotFound.js'

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/boards/:boardId" component={BoardList}/> 
          <Route excat path="/boards" component={Board} />
         
          <Route component={NotFound}/>
        </Switch>
      
      </BrowserRouter>
     
    </div>
  );
}

export default App;
