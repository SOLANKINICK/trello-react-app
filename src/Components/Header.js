import React from 'react';
import {NavLink} from 'react-router-dom';

const Header = ()=>{
    return(
        <div>
            <NavLink to="/">
                <nav class="navbar navbar-light bg-light">
                <span class="navbar-brand mb-0 h1">Trello Home</span>
                </nav>
            </NavLink>
        </div>
    );
}

export default Header;