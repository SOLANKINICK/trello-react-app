import React,{Component} from 'react'
// import EditCard from './EditCard'
import {getAllCards,deleteTheCard,postTheCard,updateTheCard} from '../Operations/Trelloapi'

export default class Cards extends Component{
    constructor(props){
        super(props)

        this.state = {
            CardsArray: [],
            isVisibleEdit: false,
            cardValue: "",
            
        }
        this.deleteCard = this.deleteCard.bind(this)
        this.addCard = this.addCard.bind(this)
        this.cancelCard = this.cancelCard.bind(this)
        this.saveCardToTheList = this.saveCardToTheList.bind(this)
        this.editCard = this.editCard.bind(this)
        this.cancelEdit = this.cancelEdit.bind(this)
        this.saveEdit = this.saveEdit.bind(this)
    }

    componentDidMount(){
        getAllCards(this.props.list.id)
        .then((cards) => {
            let tempArray = cards.map((card)=>{
                const resObj = {}
                resObj.name = card.name
                resObj.id = card.id
                resObj.desc = card.desc
                return resObj
            })
            this.setState({
                CardsArray: [...tempArray]
            })
        })
    }
  

  

   deleteCard(id){
    let cardId = id
    deleteTheCard(cardId)
    .then(()=>{
        let tempArray = [...this.state.CardsArray]
        for(let idx=0;idx<tempArray.length;idx++){
        if(tempArray[idx].id === cardId){
            tempArray.splice(idx,1);
            break;
        }
    }
        this.setState({
            CardsArray: [...tempArray]
        })
    })
    .catch((err)=>console.log(err))
   }
   
   addCard(id){
        let str = ""+id
        let addbtn = "card"+id
        document.getElementById(str).style.display = "block"
        document.getElementById(addbtn).style.display = "none"
    }
    cancelCard(id){
        let textAreaId = "txt"+id
        let addBtnId = "card"+id
        const value = document.getElementById(textAreaId).value
        if(value!=""){
            document.getElementById(textAreaId).value = ""
        }
        document.getElementById(id).style.display = "none"
        document.getElementById(addBtnId).style.display = "block"
    }

    cancelEdit(id){
        let cardId = "card"+id
        let editId = "edit"+id
        let textAreaId = "txt"+id
        const value = document.getElementById(textAreaId).value
        if(value!=""){
            document.getElementById(textAreaId).value = ""
        }
        document.getElementById(cardId).style.display = "block"
        document.getElementById(editId).style.display = "none" 
    }

    editCard(id,cardValue){
        let cardId = "card"+id
        let editId = "edit"+id
        let textAreaId = "txt"+id
        let textArea = document.getElementById(textAreaId)
        textArea.value = cardValue
        document.getElementById(cardId).style.display = "none"
        document.getElementById(editId).style.display = "block"
    }

    saveEdit(id){
        let textAreaId = "txt"+id
        let editId = "edit"+id
        const value = document.getElementById(textAreaId).value
        updateTheCard(id,value)
        .then((data)=>{
            let cardPara = document.getElementById("card"+id).children
            cardPara = cardPara[0].children
            cardPara[0].innerHTML = data.name
            document.getElementById("card"+data.id).style.display = "block"
            document.getElementById(editId).style.display = "none"
        })
    }
    saveCardToTheList(cardArray,list){
        let txtAreaId = "txt"+ list.id
        let inputText = document.getElementById(txtAreaId).value;
        postTheCard(list.id,inputText)
        .then((res)=>{
           let cardObj = {}
           cardObj.id = res.id
           cardObj.name = res.name
           cardArray.push(cardObj)
            this.setState({
                CardsArray: [...cardArray]
            });
           let s = ""+list.id
           let addbtn = "Card"+list.id
           document.getElementById(s).style.display = "none"
           document.getElementById(addbtn).style.display = "none"
        })
    }

    render(){
        if(this.state.CardsArray.length>0){
            return (
                <div className="list">
                    <h2>{this.props.list.name}</h2>
                    {this.state.CardsArray.map((card,idx)=>{
                        return (
                                <div className="d-flex flex-column">
                                    <div id={"card"+card.id} className="cards">
                                        <div className="d-flex flex-row justify-content-between" >
                                            <p className="card-text" id="txt">{card.name}</p>
                                            <div className="d-flex flex-column">
                                                <button className="del buttons" 
                                                    onClick={(event)=>this.deleteCard(card.id)}>
                                                    Delete
                                                </button>
                                                <button className="edits buttons" 
                                                    onClick={(event)=>this.editCard(card.id,card.name)}>
                                                    Edit
                                                </button>
                                            </div>
                                        </div>
                                        {/* <div className="desc" id={"desc"+card.id}>
                                            {card.desc}
                                        </div>  */}
                                    </div>
                                    <div className="edit-area flex-row justify-content-between" id={"edit"+card.id}>
                                        <div className="d-flex flex-column">
                                            <textarea rows="4" cols="30" id={"txt"+card.id}></textarea>
                                            <div className="d-flex ">
                                                <button onClick={()=>this.saveEdit(card.id,)}>
                                                    save
                                                </button>
                                                <button onClick={()=>this.cancelEdit(card.id)}>X</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        )
                    })}
                    <div className="card-add-area" id={""+this.props.list.id}>
                        <div className="d-flex flex-column">
                            <textarea rows="4" cols="30" id={"txt"+this.props.list.id}></textarea>
                            <div className="d-flex ">
                                <button onClick={()=>this.saveCardToTheList(this.state.CardsArray,this.props.list)}>
                                    save
                                </button>
                                <button onClick={()=>this.cancelCard(""+this.props.list.id)}>X</button>
                            </div>
                        </div>
                    </div>
                    <div className="addCard" id={"card"+this.props.list.id}>
                        <p onClick={()=>this.addCard(this.props.list.id)}>+ Add a card</p>
                    </div>
                   
                </div>
            )
        }
        else {
            return(
                <div className="list">
                    <h2>{this.props.list.name}</h2>
                    <div className="card-add-area" id={""+this.props.list.id}>
                        <div className="d-flex flex-column">
                            <textarea rows="4" cols="30" id={"txt"+this.props.list.id}></textarea>
                            <div className="d-flex ">
                                <button onClick={()=>this.saveCardToTheList(this.state.CardsArray,this.props.list)}>
                                    save
                                </button>
                                <button onClick={()=>this.cancelCard(""+this.props.list.id)}>X</button>
                            </div>
                        </div>
                    </div>
                    <div className="addCard" id={"card"+this.props.list.id}>
                        <p onClick={()=>this.addCard(this.props.list.id)}>+ Add a card</p>
                    </div>
                </div>
            )
        }
    }
}
