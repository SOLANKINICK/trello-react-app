import React,{Component} from 'react'
import {getAllBoards} from '../Operations/Trelloapi'
import {NavLink} from 'react-router-dom'

export default class Board extends Component{
    constructor(props){
        super(props)
         this.state = {
            BoardArray: []
         }
        //  this.state.display = this.state.display(bind)
    }
    componentDidMount(){
        getAllBoards()
        .then(Boards=>{
            let tempArray = Boards.map((board)=>
            {
                const resObj = {}
                resObj.name=board.name;
                resObj.id=board.id;
                return resObj
            })
            this.setState({
                BoardArray : [...tempArray]
            })
        })
    }
   
    render(){
        if(this.state.BoardArray.length>0){
            return (
                <div className="boardContainer">
                    {this.state.BoardArray.map((ele)=>{
                        let tempUrl = "/boards/"+ele.id;
                        return(
                            <div key = {tempUrl} id= {tempUrl} className="card-columns">
                                <div className="card ">
                                    <div className="card-body text-center">
                                        <NavLink to = {tempUrl}>
                                        <p className="card-text">{ele.name}</p>
                                        </NavLink>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            )
        }
        else{
        return(

            <div>
                {this.state.BoardArray.length<=0 && <h2>Hello board</h2>}
            </div>
        )
        }
    }
}
