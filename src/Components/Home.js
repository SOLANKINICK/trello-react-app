import React, {Component} from 'react';
import {NavLink} from 'react-router-dom'
import Header from './Header.js'

export default class Home extends Component{
    render(){
        return(
           <div>
                <Header />
                <div className="cardContainer">
                    <div class="card w-30">
                        <div class="card-body">
                            <h5 class="card-title">Get All Boards by Clicking here...</h5>
                            <NavLink to="/boards">
                                <div className="buttonClick">
                                <span class="btn btn-primary">Click</span>
                                </div>
                            </NavLink>
                        </div>
                    </div>
                </div>
           </div>

        )
    }
} 