import React,{Component} from 'react'
import Cards from './Cards'
import {getAllLists,addListToBoard} from '../Operations/Trelloapi'

export default class BoardList extends Component{
    constructor(props){
        super(props)

        this.state={
            listsArray: [],
            cardAdded: false,
            cardAddObj: {}
        }

        this.addNewList = this.addNewList.bind(this)
        this.addListToTheBoard = this.addListToTheBoard.bind(this)
    }
    componentDidMount(){
        getAllLists(this.props.match.params.boardId)
        .then((lists)=>{
            console.log(lists)
            let tempArray = lists.map((list)=>{
                let resObj = {}
                resObj.name = list.name
                resObj.id = list.id
                return resObj
            })
            this.setState({
                listsArray: [...tempArray]
            })
        })

    }

    addNewList(event){
        event.target.parentElement.style.display = "none"
        document.getElementById("title").style.display = "block"
    }

    addListToTheBoard(event){
        const boardId = this.props.match.params.boardId
        let inputId = "listTitle"+ this.props.match.params.boardId
        let child = event.target.parentElement
        let newchild = child.parentElement.children
        let value = newchild[0].value
        newchild[0].value = ""
        let parent = child.parentElement.parentElement
        parent.style.display = "none"
        parent.parentElement.children[0].style.display = "block"
        // const title = document.getElementById(inputId).value
        console.log(value)

        addListToBoard(value,boardId)
        .then(data=>{
            let tempObj = {}
            tempObj.id = data.id
            tempObj.name = data.name
            console.log(tempObj.name)
            let tempArr = [...this.state.listsArray]
            tempArr.push(tempObj)

            this.setState({
               listsArray: [...tempArr]
            })
        })
    }

    render(){
        if(this.state.listsArray.length>0){
            return(
                <div className="listContainer d-flex flex-row justify-content-between">
                    {this.state.listsArray.map((listInfo)=>{

                       return ( 
                           <div>
                                <Cards list = {listInfo}/>
                            </div>
                        )

                    })}
                    <div>
                    <div className="addList">
                        <p onClick={(event)=>this.addNewList(event)}>+Add list</p>
                    </div>
                    <div className="list-title" id="title">
                        <div className="d-flex flex-column" >
                            <input type="text" placeholder="Enter list title..." id={"listTitle"+this.props.match.params.boardId} />
                            <div className="d-flex flex-row">
                                <button onClick={(event)=>this.addListToTheBoard(event)}>Add List</button>
                                <button>X</button>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            )
        }
        return(
            <h2>Hello I'm lists Component</h2>
        )
    }
}

