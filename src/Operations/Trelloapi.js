const key = "37ecbd0116e0e4889b8381e6258f49c2"
const token = "e155ef5b5ec98c40ceec590a28560e45b400c7178381b2d1d35be03c8817cc83"


export const getAllBoards = async () => {
    const url = `https://api.trello.com/1/members/me/boards?key=${key}&token=${token}`
    const boards = await fetch(url,{
        method: 'GET'
    })
    .then((boards)=>{
        return boards.json()
    })
    .then((data)=>{
        return data
    })
    return boards
    // .then(data=>console.log(data))
    // .catch(err=>console.log(err))
}
export const getAllLists = async (id)=>{
    const idBoard = id
    const url = `https://api.trello.com/1/boards/${idBoard}/lists?key=${key}&token=${token}`
    const lists = await fetch(url, {
        method: 'GET'
    })
    .then((lists)=>{
        return lists.json()
    })
    .then((lists)=>{
        return lists
    })

    return lists
}

export const getAllCards = async (id)=>{
    const idList = id
    const url = `https://api.trello.com/1/lists/${idList}/cards?key=${key}&token=${token}`
    const cards = await fetch(url,{
        method: 'GET'
    })
    .then((cards)=>{
        return cards.json()
    })
    .then((cards)=>{
        console.log(cards)
        return cards
    })
    return cards
}

export const deleteTheCard = async (id)=>{
    const idCard = id
    console.log(idCard)
    fetch(
        `https://api.trello.com/1/cards/${idCard}?key=${key}&token=${token}`,
        {
          method: 'DELETE'
        }
    )
}

export const postTheCard = async (id,inputTxt)=>{
    const idList = id
    const data = await fetch(
        `https://api.trello.com/1/cards?idList=${idList}&name=${inputTxt}&key=${key}&token=${token}`, {
        method: 'POST'
    })
    .then(res=>res.json())
    .then(data=>data)
    .catch(err=>console.log(err))

    return data
}

export const updateTheCard = async (id,inputTxt)=>{
    const cardId = id
    const data = await fetch(`https://api.trello.com/1/cards/${cardId}?name=${inputTxt}&key=${key}&token=${token}`, {
        method: 'PUT',
        headers: {
            'Accept': 'application/json'
        }
    })
    .then(response => response.json())
    .then(data=>data)
    .catch(err => console.error(err))
    return data
}

export const addListToBoard = async(title,id)=>{
    const boardId = id
    const listTitle = title
    const data = fetch(`https://api.trello.com/1/lists?name=${listTitle}&idBoard=${boardId}&key=${key}&token=${token}`, {
        method: 'POST'
    })
    .then(response => response.json())
    .then(data=>data)
    .catch(err => console.error(err))
    return data
}